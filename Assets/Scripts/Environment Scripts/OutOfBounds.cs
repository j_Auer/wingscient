﻿using UnityEngine;
using System.Collections;

public class OutOfBounds : MonoBehaviour {

    public Transform turret;
    public Rigidbody missile;
    public Transform target;
    private Rigidbody clone;
    public float bulletSpeed = 150f; 

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Plane"))
        {
            Debug.Log("plane out of bounds"); 
            turret.LookAt(other.transform); 
			target = other.transform; 
            StartCoroutine(Shoot()); 
        }

    }

    

    private IEnumerator Shoot()
    {
        Debug.Log("Fire!");
        for (int i = 0; i < 3; i++)
        {
            clone = (Rigidbody) Instantiate(missile, turret.position, turret.rotation);
	//		HomingMissile missileScript = (HomingMissile) clone.gameObject.GetComponent(HomingMissile); 
	//		missileScript.setTarget(target); 
			HomingMissile missleScript = (HomingMissile) clone.gameObject.GetComponent<HomingMissile>(); 
			missleScript.setTarget(target); 
            clone.velocity = clone.transform.forward * bulletSpeed;

            yield return new WaitForSeconds(.15f);
        }
    }
}
