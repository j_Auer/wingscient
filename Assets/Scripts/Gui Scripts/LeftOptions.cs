﻿using UnityEngine;
using System.Collections;

public class LeftOptions : GuiState {

    public Texture2D option1;
    public Texture2D option2;
    public Texture2D option3;

    private GameController gameController;
    private PlanningState planningState; 

	Rect group = new Rect(Screen.width * .2f, Screen.height * .25f, Screen.width, Screen.height); 
	Rect box1 = new Rect(0, 0, Screen.width * .15f, Screen.height * .12f); 
	Rect box2 = new Rect(0, Screen.height * .15f , Screen.width * .15f, Screen.height * .12f); 
	Rect box3 = new Rect(0, 2 * Screen.height * .15f , Screen.width * .15f, Screen.height * .12f); 


	public LeftOptions(PlanningState planningState, GameController gameController)
	{
        this.planningState = planningState;
        this.gameController = gameController; 
        option1 = Resources.Load("JigLeft") as Texture2D; 
		option2 = Resources.Load("SweepLeft") as Texture2D;
        option3 = Resources.Load("SharpLeft") as Texture2D; 

	}

    void GuiState.OnGui()
    {
        GUI.BeginGroup(group);

		if (GUI.Button(box1, option1))
        {
		    planningState.currentState = null; 
			gameController.QueueMove(3);
			planningState.UpdateQueue(); 
        }

		if (GUI.Button(box2, option2))
        {
			planningState.currentState = null; 
			gameController.QueueMove(1);
			planningState.UpdateQueue(); 
        }

		if (GUI.Button(box3, option3))
        {
			planningState.currentState = null; 
			gameController.QueueMove(2);
			planningState.UpdateQueue(); 

        }

		GUI.EndGroup (); 
    }

}
