﻿using UnityEngine;
using System.Collections;

public class StraightOptions : GuiState {

    private GameController gameController;
    private PlanningState planningState; 

    public Texture2D option1;
    public Texture2D option2;

	Rect group = new Rect(Screen.width * .2f, Screen.height * .25f, Screen.width, Screen.height); 
	Rect box1 = new Rect(0, 0, Screen.width * .15f, Screen.height * .12f); 
	Rect box2 = new Rect(0, Screen.height * .15f , Screen.width * .15f, Screen.height * .12f); 
	Rect box3 = new Rect(0, 2 * Screen.height * .15f , Screen.width * .15f, Screen.height * .12f); 

    public StraightOptions(PlanningState planningState, GameController gameController)
    {
        this.planningState = planningState;
        this.gameController = gameController; 
        option1 = (Texture2D)Resources.Load("Straight"); 
        option2 = (Texture2D) Resources.Load("UTurn");
    }

    void GuiState.OnGui()
    {
        GUI.BeginGroup(group);

        if (GUI.Button(box2, option1))
        {
			planningState.currentState = null; 
			gameController.QueueMove(4);
			planningState.UpdateQueue(); 

        }

        if (GUI.Button(box3, option2))
        {
			planningState.currentState = null; 
			gameController.QueueMove(5);
			planningState.UpdateQueue(); 

        }


        GUI.EndGroup();
    }


}
