﻿using UnityEngine;
using System.Collections;

public class WaitState : GuiState {

	Rect textRect = new Rect (Screen.width * .33f, Screen.height * .25f, Screen.width * .45f, Screen.height * .1f); 
    void GuiState.OnGui()
    {
        GUI.BeginGroup(new Rect(Screen.width / 2 - 107, Screen.height - 150, 225, 30));
        GUI.Box(textRect, "Waiting");
        GUI.EndGroup();
                    
    }
}
