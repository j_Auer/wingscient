﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System; 

public class PreGameState : GuiState
{

    GameController gameController;
    static Rect groupRect = new Rect(Screen.width * 0.25f, Screen.height * 0.15f, Screen.width * 0.5f, Screen.height * 0.75f);
	Rect headerTextLabel = new Rect (0, 0, groupRect.width*.95f, groupRect.height * .15f); 
	Rect headerTextField = new Rect (0, groupRect.height * .05f, groupRect.width - 5f , groupRect.height * .15f); 
	Rect headerButton = new Rect (0, 2.05f * groupRect.height * .10f, groupRect.width -5f, groupRect.height * .15f); 

	Rect nameGameTextLabel = new Rect (0, 0, groupRect.width*.95f, groupRect.height * .15f); 
	Rect nameGameTextField = new Rect (0, groupRect.height * .05f, groupRect.width - 5f , groupRect.height * .15f); 
	Rect nameGameTextLabel2 = new Rect (0, 2.05f * groupRect.height * .12f, groupRect.width -5f, groupRect.height * .15f);
	Rect nameGameTextField2 = new Rect (0, 3.05f * groupRect.height * .10f, groupRect.width -5f, groupRect.height * .15f);
	Rect nameGameButton = new Rect (0, 3.05f * groupRect.height * .15f, groupRect.width -5f, groupRect.height * .15f);

	Rect joinGameButton = new Rect (0, groupRect.height * .05f, groupRect.width*.95f, groupRect.height * .15f); 
	Rect newGameButton = new Rect(0, 2.05f * groupRect.height * .10f, groupRect.width*.95f, groupRect.height * .15f); 

	Rect testButton = new Rect(Screen.width * .45f, Screen.height * .92f , Screen.width * .15f, Screen.height * .05f); 

    Rect textFieldRect = new Rect(0, 20, 200, 20); 
    string playerName = "";
    string gameName = "";
    string maxPlayers = ""; 
    bool showButtons = false;
    bool createNew = false;
	bool showHeader = false; 
    Regex rgx = new Regex("[^2-9]"); 



	float fontSize; 
	int ratio = 25; 

	GUIStyle font; 
	String fontStringOpen; 
	String fontStringClose = "</size></color>"; 

    public PreGameState(GuiController guiCon, GameController gameCon)
    {
        gameController = gameCon;
		if (PlayerPrefs.GetInt ("firstGame", 0) == 0)
						showHeader = true;
				else {
						showButtons = true; 
				playerName = PlayerPrefs.GetString("playerName", "error?"); 	
				}

		font = new GUIStyle (); 
		fontSize = (int) Mathf.Min (Screen.width, Screen.height) / ratio; 
		fontStringOpen = "<color=white><size=" + fontSize.ToString () + ">"; 
    }

    void GuiState.OnGui()
    {

        if (showHeader)
			Header ();
		else {
			if (showButtons)
					Buttons ();
			if (createNew)
					NewGame (); 
				}
    }

    private void Header()
    {
		GUI.color = Color.white; 
            GUI.BeginGroup(groupRect);
			GUI.Label(headerTextLabel, fontStringOpen + "Choose a callsign</size></color>");
			playerName = GUI.TextField(headerTextField, playerName);
		if (GUI.Button(headerButton, fontStringOpen + "OK" + fontStringClose))
            {
                PlayerPrefs.SetInt("firstGame", 1);
                PlayerPrefs.SetString("playerName", playerName);
                gameController.playerName = playerName; 
				showHeader = false; 
				showButtons = true; 
				
            }
            GUI.EndGroup();
    }

    private void Buttons()
    {
        GUI.backgroundColor = Color.white;
        GUI.BeginGroup(groupRect);
		GUI.Label(headerTextLabel, fontStringOpen + "Welcome, " + playerName + fontStringClose); 
		if (GUI.Button(joinGameButton,fontStringOpen + "Join Game" + fontStringClose))
        {
                    gameController.GetList(); 
        }
		if (GUI.Button(newGameButton, fontStringOpen + "Create Game" + fontStringClose))
        {
               createNew = true;
               showButtons = false;
		}

        GUI.EndGroup();

    }

    private void NewGame()
    {
        GUI.BeginGroup(groupRect);
		GUI.Label(nameGameTextLabel, fontStringOpen + "Name your game:" + fontStringClose);
		gameName = GUI.TextField(nameGameTextField, gameName);

		GUI.Label(nameGameTextLabel2, fontStringOpen + "Number of players" + fontStringClose);
		maxPlayers = GUI.TextField(nameGameTextField2, maxPlayers);
        maxPlayers = rgx.Replace(maxPlayers, ""); 

		if (GUI.Button(nameGameButton, fontStringOpen + "Start" + fontStringClose))
        {
           gameController.NewGame(gameName, Convert.ToInt32(maxPlayers));
           
        }

        GUI.EndGroup(); 
    }
}