﻿using UnityEngine;
using System.Collections;

public class ListGamesState : GuiState {

	GuiController guiController;
	GameController gameController; 
    Rect guiRect = new Rect(Screen.width * 0.40f, Screen.height * 0.15f, Screen.width * 0.75f, Screen.height * 0.75f);
	Rect backButton = new Rect(Screen.width * .45f, Screen.height * .92f , Screen.width * .15f, Screen.height * .05f); 
    ArrayList games; 

	public ListGamesState(GuiController guiCon, GameController gameCon, ArrayList games)
	{
		guiController = guiCon;
		gameController = gameCon;
        this.games = games; 
	}
	
	void GuiState.OnGui()
	{
        GUI.BeginGroup(guiRect);
        
        for (int i = 0; i < games.Count; i++)
        {
            GuiController.GameListing li = (GuiController.GameListing) games[i]; 
            if(GUI.Button(new Rect(0, i*40, 200, 30), li.getName())){ 
                gameController.JoinGame((int)li.getId()); 

            } 
             
        }
        GUI.EndGroup(); 

		if(GUI.Button(backButton, "Back"))
        {
            gameController.state = GameController.Gamestate.PREGAME; 
        }
	}
}
