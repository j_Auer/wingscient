﻿using UnityEngine;
using System.Collections;

public class RightOptions : GuiState {

    private GameController gameController;
    private PlanningState planningState; 

	public Texture2D option1;
	public Texture2D option2;
	public Texture2D option3;

	Rect group = new Rect(Screen.width * .2f, Screen.height * .25f, Screen.width, Screen.height); 
	Rect box1 = new Rect(0, 0, Screen.width * .15f, Screen.height * .12f); 
	Rect box2 = new Rect(0, Screen.height * .15f , Screen.width * .15f, Screen.height * .12f); 
	Rect box3 = new Rect(0, 2 * Screen.height * .15f , Screen.width * .15f, Screen.height * .12f); 

    public RightOptions(PlanningState planningState, GameController gameController)
	{
        this.planningState = planningState;
        this.gameController = gameController; 
		option1 = Resources.Load("JigRight") as Texture2D;
        option2 = Resources.Load("SweepRight") as Texture2D; 
		option3 = Resources.Load("SharpRight") as Texture2D; 

	}

	void GuiState.OnGui()
	{
        GUI.BeginGroup(group);
        
		if (GUI.Button(box1, option1))
		{
			planningState.currentState = null; 
			gameController.QueueMove(8);
			planningState.UpdateQueue(); 
			
		}
		
		if (GUI.Button(box2, option2))
		{
			planningState.currentState = null; 
			gameController.QueueMove(6);
			planningState.UpdateQueue(); 
			
		}
		
		if (GUI.Button(box3, option3))
		{
			planningState.currentState = null; 
			gameController.QueueMove(7);
			planningState.UpdateQueue(); 
			
		}

		GUI.EndGroup (); 
	}
	
}


