﻿using UnityEngine;
using System.Collections;

public class GameInfo  {

	string name; 
	int maxPlayers; 
	int numPlayers; 


	public GameInfo(string name, int max, int num)
	{
		this.name = name; 
		this.maxPlayers = max; 
		this.numPlayers = num; 
	}
}
