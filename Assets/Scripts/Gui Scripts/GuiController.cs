﻿using UnityEngine;
using System.Collections;

public class GuiController : MonoBehaviour {

    bool INGAME = false;
    bool HELPREQUEST = false;
   
	public GuiState PREGAME;
    public GuiState PLANNING;
    public GuiState PLAYING;
    public GuiState WAITING;
    public GuiState WAITINGFORPLAYERS; 
	public GuiState LISTGAMES;
    public GuiState NEWGAME; 
	public GuiState TEST; 

    public GuiState currentState;

	public Texture2D leftTexture;
	public Texture2D rightTexture;
	public Texture2D forwardTexture;
	
	protected GameController controller;
    private ArrayList games; 

	// Use this for initialization
	void Start() {


        controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        games = new ArrayList(); 

        PREGAME = new PreGameState(this, controller);
        PLANNING = new PlanningState(this, controller);
        PLAYING = new PlayState();
        WAITING = new WaitState();
        WAITINGFORPLAYERS = new WaitingForPlayersState(); 
		LISTGAMES = new ListGamesState (this, controller, games);
        CheckState(); 

	}

	void OnGUI()
    {
        currentState.OnGui(); 
    }


    void Update()
    {
        CheckState(); 
    }

    public void CheckState()
    {
        switch (controller.GetState())
        {
            case GameController.Gamestate.PREGAME:
                {
                    currentState = PREGAME;
                    break;
                }
            case GameController.Gamestate.WAIT:
                {
                    currentState = WAITING;
                    break;
                }

            case GameController.Gamestate.WAITINGFORPLAYERS:
                {
                    currentState = WAITINGFORPLAYERS;
                    break;
                }
            case GameController.Gamestate.PLAN:
                {
                    currentState = PLANNING;
                    break;
                }

            case GameController.Gamestate.PLAY:
                {
                    currentState = PLAYING;
                    break;
                }
            case GameController.Gamestate.LIST:
                {
                    currentState = LISTGAMES;
                    break;
                }
			case GameController.Gamestate.TEST:
				{
					currentState = TEST;
					break; 
				}
         }

    }
    public Texture2D GetTexture(int index)
    {

        switch(index)
        {

            case 0:
				return 	Resources.Load("nopick") as Texture2D; 
		    case 1:
				return Resources.Load("SweepLeft") as Texture2D;; 
            case 2: 
				return Resources.Load("SharpLeft") as Texture2D; 
            case 3: 
                return Resources.Load("JigLeft") as Texture2D; 
            case 4: 
                return Resources.Load("Straight") as Texture2D; 
            case 5: 
                return Resources.Load("UTurn") as Texture2D; 
            case 6:
                return Resources.Load("SweepRight") as Texture2D;
            case 7:
                return Resources.Load("SharpRight") as Texture2D;
            case 8:
                return Resources.Load("JigRight") as Texture2D; 
        }

        return Resources.Load("nopick") as Texture2D; ; 
    }


    public void addGame(string name, int id)
    {
        GameListing game = new GameListing(name, id);
        games.Add(game); 

    }

    public class GameListing
    {
        string gameName;
        int gameId;
        int maxPlayers; 

        public GameListing(string name, int id)
        {
            gameName = name;
            gameId = id;
            maxPlayers = 4; 
        }

        public string getName()
        {
            return gameName; 
        }

        public int getId()
        {
            return gameId; 
        }
    }

}
