﻿using UnityEngine;
using System.Collections;

public class PlanningState : GuiState {

    private GameController gameController;
    private GuiController guiController; 

    private Texture2D[] queue;  
    
    public GuiState currentState;

    GuiState LEFTSET;
    GuiState STRAIGHTSET; 
    GuiState RIGHTSET;
    
	Rect group = new Rect(Screen.width * .25f, Screen.height *.75f, Screen.width * .85f, Screen.height * .25f ); 
	Rect Box1 = new Rect (0, 0, Screen.width * .15f, Screen.height * .12f); 
	Rect Box2 = new Rect (Screen.width * .15f, 0, Screen.width * .15f, Screen.height * .12f);
	Rect Box3 = new Rect (2* Screen.width * .15f,  0, Screen.width * .15f, Screen.height * .12f);

	Rect Button1 = new Rect (                     6,Screen.height * .12f, Screen.width * .12f, Screen.height * .08f); 
	Rect Button2 = new Rect (   Screen.width * .17f,Screen.height * .12f, Screen.width * .12f, Screen.height * .08f);
	Rect Button3 = new Rect (2* Screen.width * .156f,Screen.height * .12f, Screen.width * .12f, Screen.height * .08f);

    public PlanningState(GuiController guiController, GameController gameController)
    {
		this.guiController = guiController;
        this.gameController = gameController; 
        LEFTSET = new LeftOptions(this, gameController);
        RIGHTSET = new RightOptions(this, gameController);
        STRAIGHTSET = new StraightOptions(this, gameController);
        queue = new Texture2D[3]; 
		
    }


    void Start()
    {
       
        currentState = null; 
    }


	void GuiState.OnGui()
    {
        UpdateQueue();
        if (currentState != null) {
						currentState.OnGui (); 
				}

        
        GUI.BeginGroup(group);

        GUI.Box(Box1, queue[0]);
        GUI.Box(Box2, queue[1]);
        GUI.Box(Box3, queue[2]);

		if (GUI.Button(Button1, guiController.leftTexture))
        {
            currentState = LEFTSET;
        }

		if (GUI.Button(Button2, guiController.forwardTexture))
        {
            currentState = STRAIGHTSET;
        }

		if (GUI.Button(Button3, guiController.rightTexture))
        {
            currentState = RIGHTSET;
        }
        GUI.EndGroup();
				
    }

   public void UpdateQueue()
    {
       
       for(int i = 0; i < 3; i++)
        {
           
                queue[i] = guiController.GetTexture(gameController.GetMove(i));
                                 
        }
    }
}
