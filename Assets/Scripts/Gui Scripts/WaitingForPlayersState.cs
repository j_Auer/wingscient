﻿using UnityEngine;
using System.Collections;

public class WaitingForPlayersState : GuiState {

    Rect guiRect = new Rect(Screen.width * 0.4f, Screen.height*.25f, Screen.width * 0.6f, Screen.height * 0.75f);

    void GuiState.OnGui()
    {
        GUI.backgroundColor = Color.white;
        GUI.BeginGroup(guiRect);
        GUI.Label(new Rect(0, 0, 150, 30), "Waiting for players"); 


        GUI.EndGroup();
    }
}
