﻿using UnityEngine;
using System.Collections;

public class HomingMissile : MonoBehaviour {

	public int Power = 20;
	public GameObject explosion;
	public AudioClip explosionSound;
	public float timer = 20f; 
	private Transform target; 
	public float blah = 0f; 
	public float speed = 150f; 
	
	void OnTriggerEnter(Collider other)
	{
		Debug.Log("Missile hit " + other.name); 
		if(other.tag == "Plane")
		{
			
			other.gameObject.SendMessage("Damage", Power, SendMessageOptions.DontRequireReceiver);
			GameObject clone;
			clone = (GameObject) Instantiate(explosion, transform.position, transform.rotation);
			AudioSource.PlayClipAtPoint(explosionSound, transform.position); 
			Destroy(gameObject);
			
		}
	}

	void Update()
	{
		Debug.Log ("Target: " + target.ToString ()); 
		transform.LookAt (target); 
		transform.Translate(Vector3.forward * speed * Time.deltaTime); 
	}

	public void setTarget(Transform target)
	{
		this.target = target; 
	}
}
