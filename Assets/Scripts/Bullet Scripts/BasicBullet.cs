﻿using UnityEngine;
using System.Collections;

public class BasicBullet : MonoBehaviour {

	public int Power = 2;
    public GameObject explosion;
    public AudioClip explosionSound;
    public float timer = 20f; 
	

	void OnTriggerEnter(Collider other)
	{
       Debug.Log("Bullet hit " + other.name); 
		if(other.tag == "Plane")
		{
            
			other.gameObject.SendMessage("Damage", Power, SendMessageOptions.DontRequireReceiver);
            GameObject clone;
            clone = (GameObject) Instantiate(explosion, transform.position, transform.rotation);
            AudioSource.PlayClipAtPoint(explosionSound, transform.position); 
            Destroy(gameObject);
            
		}
	}


    void Start()
    {
      // Destroy(gameObject, timer); 
    }
}
