﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	float height = 20.0f; 
	float distance = 15.0f; 
	public Transform target; 

	void Start()
	{
			}

	void LateUpdate () {


		if(target)
		{	
			Vector3 tp = target.position; 
			tp.y = height; 
			transform.position = tp; 
			transform.LookAt(tp); 
		}
}


	public void AquireTarget(Transform tar)
	{
		target = tar.Find("Ship"); 
		transform.rotation = Quaternion.Euler (new Vector3 (90, tar.eulerAngles.y));   

	}
}