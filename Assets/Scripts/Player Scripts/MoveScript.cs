﻿using UnityEngine;
using System.Collections;

public class MoveScript : MonoBehaviour
{
    
    public int health = 40;
   
    public GameObject explosion;
    public AudioClip explosionSound;
    public AudioClip shootSound; 
    public float bulletSpeed = 150f; 
    public GameObject parent;
    public Rigidbody bullet; 
    public Transform muzzle;
	
    private Rigidbody clone; 
    private Quaternion relRot;
    private GameController gameController;

    public bool enemyInRange = false;
    private bool bulletFired = false;

    private string manueverClass = "_A";

 //   private ArrayList moves; 
	private int move1, move2, move3; 
	private int[] moves; 
	int queued; 

    void Start()
    {
		moves = new int[]{0, 0, 0}; 
		queued = 0; 

     //   moves = new ArrayList(); 
        relRot = this.transform.rotation * Quaternion.Inverse(parent.transform.rotation);
        updateParent(); 
    }

    void Awake()
    {
		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
   
           
    }

    // Update is called once per frame
    void Update()
    {
		if (health < 0) 
		{
            Die(); 
		}

		TestShoot ();
		if (!animation.isPlaying){
			animation.Play("idle" + manueverClass);
		}       
    }

    public void Die()
    {
        Debug.Log("I'm dying!");
        GameObject clone;
        clone = (GameObject) Instantiate(explosion, transform.position, transform.rotation);
        AudioSource.PlayClipAtPoint(explosionSound, transform.position); 
		Transform camTarget = transform.Find("CameraTarget"); 
		camTarget.parent = null; 
        Destroy(gameObject); 
    }
    
 
    public void updateParent()
    {  
          
        parent.transform.position = this.transform.position;
        parent.transform.rotation = relRot * this.transform.rotation; 
        transform.localPosition = Vector3.zero; 
        animation.PlayQueued("idle" + manueverClass); 
        
    }

    public void endMoves()
    {
		Debug.Log ("end idle called"); 
        gameController.PlayedMoves(); 
    }


    public void clearMoves()
    {
		for(int i = 0; i < 3 ; i++)
		{
			moves[i] = 0; 
		}
		queued = 0; 
    }

	public void queueEnemyMove(int move)
	{
		moves [queued] = move; 
		queued++; 
	}

    public void queueMove(int move)
    {
		moves [queued] = move; 
		queued++; 
		///moves.Add(move); 
        if(queued == 3)
        {
            gameController.SetMoves(moves); 
        }
    }

    public int getMove(int i)
    {
        if(moves.Length >= i + 1)
            return (int)moves[i];
        else return 0; 
    }
    

    public void reset()
    {
    
    }

    public void runMoves()
    {
        foreach(int move in moves)
        {
            runMove(move); 
        }
		runMove (10); 
		clearMoves (); 
    }

 
    public void runMove(int move)
    {
        switch (move)
        {
            case 0:                    
                break; 
            case 1: animation.PlayQueued("wideLeft" + manueverClass);
                    animation.PlayQueued("idleShoot" + manueverClass); 
				break;
            case 2: animation.PlayQueued("tightLeft" + manueverClass);
                    animation.PlayQueued("idleShoot" + manueverClass);
                break;
            case 3: animation.PlayQueued("swerveLeft" + manueverClass);
                    animation.PlayQueued("idleShoot" + manueverClass);
                break;
            case 4: animation.PlayQueued("forward" + manueverClass);
                    animation.PlayQueued("idleShoot" + manueverClass); 
				break;
            case 5: animation.PlayQueued("UTurn" + manueverClass);
                    animation.PlayQueued("idleShoot" + manueverClass);
                break;
            case 6: animation.PlayQueued("wideRight" + manueverClass);
                    animation.PlayQueued("idleShoot" + manueverClass); 
				break;
            case 7: animation.PlayQueued("tightRight" + manueverClass);
                    animation.PlayQueued("idleShoot" + manueverClass);
                break;
            case 8: animation.PlayQueued("swerveRight" + manueverClass);
                    animation.PlayQueued("idleShoot" + manueverClass);
                break;
            case 9: animation.PlayQueued("idle" + manueverClass);
				break; 
			case 10: animation.PlayQueued("endIdle" + manueverClass); 
				break;

           

        }

    }

    private IEnumerator Shoot()
    {

        if (enemyInRange)
        {
            bulletFired = true;
            for (int i = 0; i < 3; i++)
            {
                
                clone = (Rigidbody)Instantiate(bullet, muzzle.position, muzzle.rotation);
                clone.velocity = clone.transform.forward * bulletSpeed;
                AudioSource.PlayClipAtPoint(shootSound, muzzle.position);
                yield return new WaitForSeconds(.15f); 

            }
			enemyInRange = false; 
         }
        
    }

	private void TestShoot()
	{
		if (Input.GetKeyDown("space") && this.transform.parent.gameObject.name == "Player") 
		{
			bulletFired = true;
			clone = (Rigidbody)Instantiate(bullet, muzzle.position, muzzle.rotation);
			//clone.velocity = clone.transform.forward * bulletSpeed;
            clone.rigidbody.AddForce(clone.transform.forward * 6500); 
			// AudioSource.PlayClipAtPoint(shootSound, muzzle.position);
		}
	}

	private void Damage(int dmg)
	{
		health -= dmg; 
	}
}
