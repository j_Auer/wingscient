﻿using UnityEngine;
using System.Collections;

public class PlayerEnemyDetection : MonoBehaviour
{

     private MoveScript moveScript;

    void Awake()
    {
		
        moveScript = transform.parent.GetComponentInChildren<MoveScript>(); 
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Plane"))
        {
            Debug.Log(other.gameObject.name + " Detected");
            moveScript.enemyInRange = true;
        }

    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Plane"))
        {
            Debug.Log(other.gameObject.name + " gone");
            moveScript.enemyInRange = false;
        }
    }


}
