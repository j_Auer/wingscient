﻿using UnityEngine;
using System.Collections;

public class Player {

    public int playerNum { get; set; }
    public string playerName { get; set; }
    public MoveScript myPlane; 


    public Player(string name, int num)
    {
        playerName = name;
        playerNum = num; 
    }

	public void setScript(MoveScript script)
    {
        myPlane = script; 
    }
	
	// Update is called once per frame
	void Update () {
	
	}
	public void queueEnemyMove(int move)
	{
		myPlane.queueEnemyMove (move); 
	}

    public void queueMove(int move)
    {
        myPlane.queueMove(move); 
    }

    public int getMove(int i)
    {
       return myPlane.getMove(i); 
    }
    public void runMoves()
    {
        myPlane.runMoves(); 
    }
}
