using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;  

public class GameController : MonoBehaviour {

	public Coms com; 
    public int gameId = 0; 
	public int playerID = 0;
    public int numPlayers = 0;
    public int maxPlayers = 0; 
    public string playerName;
    private string gameName;

    public Transform spawn1;
    public Transform spawn2;
    public Transform spawn3;
    public Transform spawn4;
    public Transform spawn5;
    public Transform spawn6;
    public Transform spawn7;
    public Transform spawn8;

    public GameObject playerPrefab; 

    public enum Gamestate { LIST, WAIT, WAITINGFORPLAYERS, PLAN, PLAY, PLAYING, PREGAME, GAMEOVER, TEST };
    public Gamestate state;
    
    public bool opReady = false;
    public bool meReady = false; 

	private int activeQueue = 0;

    private Dictionary<int, Player> players; 
	
    void Awake () {

        playerName = PlayerPrefs.GetString("playerName", ""); 
        com =  GetComponent<Coms>(); 
        state = Gamestate.PREGAME; 
      //PlayerPrefs.DeleteAll(); 
        players = new Dictionary<int, Player>(); 
	}

    public void SetName(String name)
    {
        PlayerPrefs.SetString("playerName", name); 
        playerName = name;
        PlayerPrefs.SetInt("nameSet", 1); 
    }

	public Gamestate GetState()
	{
		return state; 
	} 

    public int GetGameID()
    {
        return gameId;
    }

    public int GetPlayerID()
    {
        return playerID; 
    }

    public string GetPlayerName()
    {
        return playerName; 
    }


    //player methods 

    public void QueueMove(int move)
    {
        players[playerID].queueMove(move); 
    }


    public int GetMove(int i)
    {
        return players[playerID].getMove(i); 
    }

   	public void LockMoves()
    {
        state = Gamestate.WAIT; 
    }

    public void PlayedMoves()
    {
		com.RanMoves (); 
    }
    
    public void ClientPlayerSetup(int pNum, int gNum, int numP, int maxP)
    {
            playerID = pNum;
            gameId = gNum;
            numPlayers = numP;
            maxPlayers = maxP;
            AddPlayer(playerName, pNum); 
			//SetCamera (pNum); 
	}

	public void SetCamera(int pNum)
	{
		Player player = players [pNum]; 
		var targetTransform = GameObject.Find(pNum.ToString()).transform; 
		var target = targetTransform.Find ("Ship").transform;
		Camera cam = Camera.main; 

		cam.GetComponent<CameraScript> ().target = target; 


	}

    public void AddPlayer(string pName, int pNum)
     {
		if(!players.ContainsKey(pNum))
		{
	         Player newPlayer = new Player(pName, pNum);
	         GameObject spawnPoint = GameObject.Find(pNum.ToString()); 
	         
	         GameObject spacePlayer;
	         spacePlayer = (GameObject) Instantiate(playerPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation); 
	         newPlayer.setScript(spacePlayer.GetComponentsInChildren<MoveScript>()[0]); 
	         players[pNum] = newPlayer; 
			spacePlayer.name = pNum.ToString(); 
			if(pNum == playerID)
			{
				Camera cam = Camera.main; 
				
				//cam.GetComponent<CameraScript> ().target = spacePlayer.transform.Find("Ship"); 
				cam.GetComponent<CameraScript>().AquireTarget(spacePlayer.transform);

			}


		}

     }

    public void UpdatePlayerQueue(int playerNum, int move1, int move2, int move3)
    {
        players[playerNum].queueEnemyMove(move1);
		players[playerNum].queueEnemyMove(move2);
		players[playerNum].queueEnemyMove(move3); 
    }

    public void RunMoves()
    {
        foreach(KeyValuePair<int, Player> p in players)
        {
            p.Value.runMoves(); 
        }
    }

    //communication calls

    public void NewGame(string gameName, int maxPlayers)
    {
        com.NewGame(gameName, maxPlayers, playerName); 
    }
       
    public void GetList()
    {
        com.GetList();
        state = Gamestate.LIST; 
    }
    
    public void JoinGame()
    {
        com.JoinGame();
    }

    public void JoinGame(int id)
    {
        com.JoinGame(id);
    }

    public void List()
    {
        state = Gamestate.LIST;
    }

    public void SetMoves(int[] moves)
    {
        com.QueueMoves(moves); 
    }




}
