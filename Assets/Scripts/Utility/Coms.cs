﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON; 


public class Coms : MonoBehaviour
{

    private static string   URL = "http://67.189.125.123:3005/";
    private string HELP = URL + "manage?req=help";
    private string LIST = URL + "manage?req=list";
    private string JOIN;
    private string STATE;
    private string QUEUE;
    private string PLAYED;
	private string STATUS; 
	private string PLAYERLIST; 
	private string MOVELIST; 
	private string RANMOVES; 

    public string response;
    private static object myLock = new object();
    GameController controller;
    GuiController guiCon;
	bool gotMoves = false; 

    bool setup = false;

    void Start()
    {
        controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        guiCon = GameObject.FindGameObjectWithTag("GameController").GetComponent<GuiController>(); 
	
    }

    public void BuildURLS()
    {
      
       
		STATUS = URL + "game?req=status&gameID=" + controller.GetGameID() + "&playerID=" + controller.GetPlayerID();
		STATE = URL + "game?req=state&gameID=" + controller.GetGameID() + "&playerID=" + controller.GetPlayerID();
        QUEUE = URL + "game?req=queue&gameID=" + controller.GetGameID() + "&playerID=" + controller.GetPlayerID();
        PLAYED = URL + "game?req=played&playerName=" + controller.GetPlayerName() + "&gameID=" + controller.GetGameID();
		PLAYERLIST = URL + "game?req=players&playerID=" + controller.GetPlayerID() + "&gameID=" + controller.GetGameID();
		MOVELIST = URL + "game?req=moves&playerID=" + controller.GetPlayerID() + "&gameID=" + controller.GetGameID();
		RANMOVES = URL + "game?req=ranMoves&playerID=" + controller.GetPlayerID() + "&gameID=" + controller.GetGameID();
        setup = true; 

    }

	public void GetList()
	{
		Debug.Log("Sending: " + LIST);
		WWW req = new WWW (LIST); 
		StartCoroutine (WaitForRequest (req)); 
	}

	public void RequestMoveList()
	{
		Debug.Log("Sending: " + MOVELIST);
		WWW req = new WWW (MOVELIST); 
		StartCoroutine (WaitForRequest (req)); 
	}

    public void JoinGame()
    {
		Debug.Log("Sending: " + JOIN);
		WWW req = new WWW (JOIN);
		StartCoroutine (WaitForRequest (req));
	}

    public void NewGame(string gameName, int maxPlayers, string playerName)
    {
		Debug.Log("Sending: newGame");
        WWW req = new WWW(URL + "manage?req=new" + "&maxPlayers=" + maxPlayers + "&gameName=" + gameName + "&playerName=" + playerName);
        StartCoroutine(WaitForRequest(req));

    }

   public void RanMoves()
	{
		Debug.Log("Sending: " + RANMOVES);
		WWW req = new WWW (RANMOVES); 
		StartCoroutine (WaitForRequest (req)); 
	}

    public void JoinGame(int id)
    {

        JOIN = URL + "manage?req=join&playerName=" + controller.GetPlayerName() + "&gameID=" + id.ToString();
		Debug.Log("Sending: " + JOIN);
		WWW req = new WWW(JOIN);
        StartCoroutine(WaitForRequest(req));
    }

	public void requestPlayerList()
	{
		Debug.Log("Sending: " + PLAYERLIST);
		WWW req = new WWW(PLAYERLIST);
		StartCoroutine(WaitForRequest(req));
	}
    public void QueueMoves(int[] moves)
    {
		int move1 = (int)moves[0];
        int move2 = (int)moves[1];
        int move3 = (int)moves[2];
        string moveString = "&m1=" + move1.ToString() + "&m2=" + move2.ToString() + "&m3=" + move3.ToString(); 
        WWW req = new WWW(QUEUE + moveString);
		Debug.Log ("Sending: " + QUEUE); 
        StartCoroutine(WaitForRequest(req));
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        ProcessResponse(www.text); 
	    Debug.Log ("response received: " + www.text);
    }

    public void StartUpdating()
    {
        InvokeRepeating("GetState", 0, 3); 
    }

	public void PingStatus()
	{
		InvokeRepeating ("GetStatus", 0, 3); 
	}

	public void GetStatus()
	{
		Debug.Log("Sending: " + STATUS);
		WWW req = new WWW (STATUS); 
		StartCoroutine (WaitForRequest (req)); 
	}

    public void GetState()
    {
		Debug.Log("Sending: " + STATE);
		WWW req = new WWW(STATE);
        StartCoroutine(WaitForRequest(req));
    }

    public void StopUpdating()
    {
        CancelInvoke("getState"); 
    }
    
    public void ProcessResponse(String msg)
    {
        var response = JSON.Parse(msg); 
        String status = response["reply"];
		JSONArray playerList; 
		JSONArray gamesList; 

		int pNum, gNum, numP, maxP;  


        switch(status)
        {

            case "new": 
            case "joined":
                pNum = Convert.ToInt32(response["playerID"]); 
                gNum = Convert.ToInt32(response["gameID"]);
                maxP = Convert.ToInt32(response["maxPlayers"]);
                numP = Convert.ToInt32(response["numPlayers"]);
                controller.ClientPlayerSetup(pNum, gNum, numP, maxP);
                BuildURLS(); 
			    controller.state = GameController.Gamestate.WAITINGFORPLAYERS;
			    PingStatus(); 
                break; 

            case "list":
                gamesList = (JSONArray) response["games"]; 
                foreach(JSONNode node in gamesList )
                {
                    string gameName = node["name"];
                    int gameID = Convert.ToInt32(node["id"]);
                    guiCon.addGame(gameName, gameID); 
                }
                controller.state = GameController.Gamestate.LIST; 
                break; 

            case "playMoves":
				RequestMoveList(); 
               
                break; 
			case "moves": 
				playerList = (JSONArray)response["moves"];
				foreach (JSONNode node in playerList)
				{
					int currentPlayer = Convert.ToInt32(node["playerID"]); 
					if(currentPlayer != controller.playerID) 
				   {
						controller.UpdatePlayerQueue(currentPlayer, Convert.ToInt32(node["m1"]), Convert.ToInt32(node["m2"]), Convert.ToInt32(node["m3"])); 
					}
				}
				controller.RunMoves(); 
				break; 
			case "lockedMoves": 
				controller.state = GameController.Gamestate.PLAY; 
				PingStatus(); 
				break; 

            case "waitingForPlayers":
                controller.state = GameController.Gamestate.WAITINGFORPLAYERS;
				PingStatus(); 
                break; 

			case "waiting":
				PingStatus(); 
				break; 
			          
            case "gameFull":
				requestPlayerList(); 
				break; 

            case "queueMoves":
                controller.state = GameController.Gamestate.PLAN;
                break; 

			case "status": 
				string stat = response["statusCurrent"]; 
				if(stat.Equals("false"))
				{
					CancelInvoke(); 
					GetState(); 
				}
				string message = response["message"]; 
				if(message.Equals("playerJoined"))
				{
					requestPlayerList(); 
				}
				break;  

			case "playerList": 
					playerList = (JSONArray)response["players"];
					foreach(JSONNode node in playerList)
					{
						controller.AddPlayer(node["playerName"], Convert.ToInt32(node["playerID"])); 
					}
			break; 
        }

    }

}
