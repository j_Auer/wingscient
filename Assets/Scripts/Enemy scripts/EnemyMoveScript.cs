﻿using UnityEngine;
using System.Collections;

public class EnemyMoveScript : MonoBehaviour {

    
    
    public float bulletSpeed = 85f; 
    public GameObject parent;
    public Rigidbody bullet; 
    public Transform muzzle;

    public int health = 20; 

    private Rigidbody clone; 
    private Quaternion relRot;
    private GameController gameController;
    public bool enemyInRange = false; 
    private bool bulletFired = false;


    void Start()
    {
        relRot = this.transform.rotation * Quaternion.Inverse(parent.transform.rotation);
        updateParent(); 
    }

    void Awake()
    {
        gameController = GetComponent<GameController>();


    }
    // Update is called once per frame
    void Update()
    {
        if(health < 0)
        {
            Destroy(gameObject); 
        }
    }

    public void leftTurn()
    {
        transform.localPosition = Vector3.zero; 
        animation.Play("leftTurn");
    }

    public void forward()
    {
        animation.Play("forward");
    }

    public void rightTurn()
    {
        animation.Play("rightTurn"); 
    }
    public void idle()
    {
        animation.Play("idle");
    } 

    public void updateParent()
    {  
          
        parent.transform.position = this.transform.position;
        parent.transform.rotation = relRot * this.transform.rotation; 
        transform.localPosition = Vector3.zero; 
        animation.PlayQueued("idle"); 
        
    }

    public void endMoves()
    {
        gameController.PlayedMoves(); 
    }

    public void reset()
    {
    }

    public void runMove(int move)
    {
        switch (move)
        {
            case 1: animation.PlayQueued("leftTurn"); 
				Shoot ();
                break;
            case 2: animation.PlayQueued("forward");
				Shoot ();
                break;
            case 3: animation.PlayQueued("rightTurn");
				Shoot ();
                break;
            case 4: animation.PlayQueued("endIdle");
				Shoot ();
                break; 
            case 5:
                break; 

        }

		 

    }

    void Shoot()
    {


        if (enemyInRange)
        {
           
			for( int i = 0; i < 3; i++)
			{
                clone = (Rigidbody) Instantiate(bullet, muzzle.position, muzzle.rotation);
                clone.velocity = clone.transform.forward * bulletSpeed;
               // AudioSource.PlayClipAtPoint(shootSound, muzzle.position);
			}

         }
        
    }

    private void Damage(int dmg)
    {
        Debug.Log("taking " + dmg + " dmg"); 
        health -= dmg; 
    }
   
}
