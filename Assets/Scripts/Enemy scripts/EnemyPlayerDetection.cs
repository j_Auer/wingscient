﻿using UnityEngine;
using System.Collections;

public class EnemyPlayerDetection : MonoBehaviour {

    private GameObject enemy;
    private MoveScript moveScript; 

    void Awake()
    {
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        //moveScript = enemy.GetComponentInChildren<MoveScript>();
        moveScript = transform.parent.GetComponent<MoveScript>(); 
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log(other.gameObject.name + " Detected");
            moveScript.enemyInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log(other.gameObject.name + " gone");
            moveScript.enemyInRange = false;
        }
    }
}
