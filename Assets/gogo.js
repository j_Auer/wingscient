﻿public var speed: float = 5.0; // linear speed in m/s
public var turnSpeed: float = 1.5; // rotation speed control
 
var target: Vector3; // assign the target position to this variable
 
function Update(){
  // update direction each frame:
  var dir: Vector3 = target - transform.position;
  // calculate desired rotation:
  var rot: Quaternion = Quaternion.LookRotation(dir);
  // Slerp to it over time:
  transform.rotation = Quaternion.Slerp(transform.rotation, rot, turnSpeed * Time.deltaTime);
  // move in the current forward direction at specified speed:
  transform.Translate(Vector3(0, 0, speed * Time.deltaTime));
}